OpenAid Partners 

Feature Dependencies:

Modules Used:
link - http://drupal.org/project/link
views - http://drupal.org/project/views

Description:
The Atom Partners module creates a Partner node type with fields to track the partner's name, description, logo and website. The module also creates views to display blocks of partner logos and names that can be displayed in the site sidebar. It also contains a context to load the partner info block provided by the accompanying Partners Extra module.

Caveats:

